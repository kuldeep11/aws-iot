import json
import requests
from aws import service
from config import config


def runServices():
    try:
        backend_uri = config.BACKEND_URI
        scheduled_task = requests.get(backend_uri + "/api/get_scheduled_task")
        records = scheduled_task.json()
        # requests.post(config.BACKEND_URI + '/' + 'create_user_thing' + '/' + str(1), json.dumps({'user_id': 'user_id', 'thingName': 'thingName'}))
        print ("Scheduled Records - ", records)
        if (len(records.get('serviceRequest')) > 0):
            for rec in records.get('serviceRequest'):
                if not rec.get('operation') or not rec.get('task'):
                    continue
                task = rec.get('operation') + rec.get('task')
                service.getAWSResponse(task, rec)
    except Exception as e:
        print ("=>", e)
