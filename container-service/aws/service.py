import boto3
import json
import logging
import http.client
import requests
from config import config
import os
import sys

client = boto3.client('iot', region_name=os.environ['AWS_REGION_NAME'], aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
                      aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])
resource = boto3.client('resource-groups', region_name=os.environ['AWS_REGION_NAME'], aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
                      aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])
# client = boto3.client('iot')
# resource = boto3.client('resource-groups')
logging.basicConfig(filename='logger.log',
                    format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
url = config.BACKEND_URI
url_create_user_thing = 'create_user_thing'
url_delete_user_thing = 'delete_user_thing'


def getAWSResponse(task, serviceRequest):

    task_function = getattr(sys.modules[__name__], task)
    print("Executing IOT Commands....")
    url = config.BACKEND_URI
    api_endPoint = '/api/service_request/' + str(serviceRequest['jobId'])
    user_id = serviceRequest['userId']
    uuid = serviceRequest['uuid']
    aws_response = task_function(
        user_id, uuid, eval(serviceRequest['attrData']))
    print("AWS Response => ", aws_response)
    if aws_response['ResponseMetadata']['HTTPStatusCode'] == 200:
        data = {
            "processStatus": "done",
            "response": aws_response['ResponseMetadata']['message']
        }
        json_data = json.dumps(data)
        r = requests.put(url + api_endPoint, data=json_data)
    else:
        retryCount = serviceRequest['retryCount'] + 1
        data = {
            "retryCount": retryCount,
            "processStatus": "retry"
        }
        if retryCount >= 5:
            data.update({"processStatus": "failed"})
        json_data = json.dumps(data)
        r = requests.put(url + api_endPoint, data=json_data)


def createthingtype(user_id, uuid, attrData):
    try:
        tags = attrData['tags']
        tags.append({"Key": "Processing", "Value": uuid})
        response = client.create_thing_type(
            thingTypeName=attrData['thingTypeName'],
            thingTypeProperties=attrData['thingTypeProperties'],
            tags=tags
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def deprecatethingtype(user_id, uuid, attrData):
    try:
        response = client.deprecate_thing_type(
            thingTypeName=attrData['thingTypeName'],
            undoDeprecate=True
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}

    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def deletethingtype(user_id, uuid, attrData):
    try:
        response = client.delete_thing_type(
            thingTypeName=attrData['thingTypeName']
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}

    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def creatething(user_id, uuid, attrData):
    try:
        thing_type = client.list_thing_types(
            maxResults=123,
            thingTypeName=attrData['thingTypeName']
        )
        if (len(thing_type['thingTypes']) <= 0):   # check if thingtype exist or not
            thingTypeAttrData = {
                    "thingTypeName": attrData['thingTypeName'],
                    "thingTypeProperties": {
                        "thingTypeDescription": attrData['thingTypeName'],
                        "searchableAttributes": [attrData['thingTypeName']]
                    },
                    "tags": []
                }
            createthingtype(user_id, uuid, thingTypeAttrData)
        attributePayload = attrData['attributePayload']
        attributePayload['attributes'].update({"Processing": uuid})
        attributePayload.update({"merge": True})
        response = client.create_thing(
            thingName=attrData['thingName'],
            thingTypeName=attrData['thingTypeName'],
            attributePayload=attributePayload
        )
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            thingName = response['thingName']
            requests.post(url + '/' + url_create_user_thing + '/' + str(1), json.dumps({'user_id': 1, 'thingName': thingName}))
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def updatething(user_id, uuid, attrData):
    try:
        attributePayload = attrData['attributePayload']
        attributePayload.update({"merge": True})
        response = client.create_thing(
            thingName=attrData['thingName'],
            thingTypeName=attrData['thingTypeName'],
            attributePayload=attributePayload,
            expectedVersion=attrData['expectedVersion'],
            removeThingType=False
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def deletething(user_id, uuid, attrData):
    try:
        response = client.delete_thing(
            thingName=attrData['thingName'],
            expectedVersion=attrData['expectedVersion']
        )
        requests.post(url + '/' + url_delete_user_thing + '/' + str(1), json.dumps({'user_id': 1, 'thingName': attrData['thingName']}))
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def createthinggroup(user_id, uuid, attrData):
    try:
        attributePayload = attrData['attributePayload']
        attributePayload['attributes'].update({"Processing": uuid})
        attributePayload.update({"merge": True})
        tags = attrData['tags']
        tags = [{"Key": "Processing", "Value": uuid}]
        if attrData.get('parentGroupName'):
            response = client.create_thing_group(
                thingGroupName=attrData['thingGroupName'],
                parentGroupName=attrData['parentGroupName'],
                thingGroupProperties={
                    'thingGroupDescription': attrData['thingGroupDescription'],
                    'attributePayload': attrData['attributePayload']
                },
                tags=tags
            )
            return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
        else:
            response = client.create_thing_group(
                thingGroupName=attrData['thingGroupName'],
                thingGroupProperties={
                    'thingGroupDescription': attrData['thingGroupDescription'],
                    'attributePayload': attrData['attributePayload']
                },
                tags=tags
            )
            return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def updatethinggroup(user_id, uuid, attrData):
    try:
        attributePayload = attrData['attributePayload']
        attributePayload.update({"merge": True})
        response = client.update_thing_group(
            thingGroupName=attrData['thingGroupName'],
            thingGroupProperties={
                'thingGroupDescription': attrData['thingGroupDescription'],
                'attributePayload': attrData['attributePayload']
            }
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}


def deletethinggroup(user_id, uuid, attrData):
    try:
        attributePayload = attrData['attributePayload']
        attributePayload.update({"merge": True})
        response = client.delete_thing_group(
            thingGroupName=attrData['thingGroupName'],
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}
    
def creategroup(user_id, uuid, attrData):
    try:
        tags = attrData['tags']
        tags.update({"Processing": uuid})
        response = resource.create_group(
            Name=attrData['Name'],
            Description=attrData['Description'],
            ResourceQuery=attrData['ResourceQuery'],
            Tags=tags
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}

def updategroup(user_id, uuid, attrData):
    try:
        response = resource.update_group(
            GroupName=attrData['GroupName'],
            Description=attrData['Description']
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}
    
def deletegroup(user_id, uuid, attrData):
    try:
        response = resource.delete_group(
            GroupName=attrData['GroupName']
        )
        return {'ResponseMetadata': {'HTTPStatusCode': 200, 'message': 'Successful'}}
    except Exception as err:
        logging.error(err)
        print("#err", err)
        return {'ResponseMetadata': {'HTTPStatusCode': 401, 'message': err}}
    