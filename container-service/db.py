import pymysql
from config import config

class Database:
    def __init__(self):
        host = config.BACKEND_URI
        user = config.DB_USER
        password = config.DB_PASSWORD
        db = config.DB_NAME
        self.con = pymysql.connect(host=host, user=user, password=password, db=db, cursorclass=pymysql.cursors.
                                   DictCursor)
        self.cur = self.con.cursor()
