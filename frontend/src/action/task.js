import api from './api'
import Constants from '../constants'

export const uData = (url)=> {
    return function (dispatch) {
        api.get(url).then(res=> {
            dispatch({
                type : Constants.GET_LIST_OF_TASKS_SUCCESS,
                payload : res.data
            })
        }).catch(err=>{
            dispatch({
                type : Constants.GET_LIST_OF_TASKS_ERROR,
                payload : false
            })
        })
    }
}

export const addTask = (url,data,oper)=> {
    // console.log("aaaaaa",dataa)
    return function (dispatch) {
        const url1= `${url}?task=${data}&operation=${oper}`
        console.log("url1",url1)
        api.get(url1).then(res=> {
            const resp = res.data
            console.log("222........",res)
            dispatch({
                type : Constants.ADD_TASK_SUCCESS,
                // payload : res.data,
                payload: {data , oper , resp}
                // taskName: data,
                // operation:oper


            })
        }).catch(err=>{
            console.log("333.........",err)
            dispatch({
                type : Constants.ADD_TASK_ERROR,
                payload : false
            })
        })
    }
}

export const oncreatetask = (url)=> {
    // console.log("aaaaaa",dataa)
    return function (dispatch) {
        // const url1= `${url}?task=${data}&operation=${oper}`
        // console.log("url1",url1)
        api.post(url).then(res=> {
            const resp = res.data
            console.log("222........",res)
            dispatch({
                type : Constants.POST_ONCREATE_SUCCESS,
                // payload : res.data,
                payload: {resp}

            })
        }).catch(err=>{
            console.log("333.........",err)
            dispatch({
                type : Constants.POST_ONCREATE_FAIL,
                payload : false
            })
        })
    }
}