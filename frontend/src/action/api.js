import axios from 'axios'
export const ApiConfig = {
    apiBaseUrl : "http://13.52.132.85:8089",
}
const api = {
    post: (url, data) => {
        return axios.post(ApiConfig.apiBaseUrl + url, data)
    },

    get: (url) => { 
        return axios.get(ApiConfig.apiBaseUrl + url)
    },

    put: (url, data) => {
            return axios.put(ApiConfig.apiBaseUrl + url, data)
    },

    delete: (url) => {
        return axios.delete(ApiConfig.apiBaseUrl + url)
    }
}

export default api