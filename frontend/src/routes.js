import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Task = React.lazy(() => import('./views/Task'));
const addNew = React.lazy(() => import('./views/addNew'));





// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/task', name: 'Task', component: Task },
  { path: '/addnew', name: 'addNew', component: addNew },

];

export default routes;
