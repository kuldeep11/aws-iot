import React, { Component } from 'react';
import ServerTable from 'react-strap-table';
import Modal from 'react-modal';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {addTask} from '../../action/task';
import {uData} from '../../action/task';
import {ApiConfig} from '../../action/api';


const customStyles = {
  content: {
    top: '20%',
    left: '35%',
    right: '40%',
    bottom: '35%'
  }
};

class Task extends Component {
  constructor() {
    super();
    this.state = {
      modalIsOpen: false,
      task : '',
      operation: '',
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.getTask = this.getTask.bind(this);
    this.getOperation = this.getOperation.bind(this);
  }
  

  componentDidMount() {
    this.props.uData('/api/service_request')
  }

  openModal(){
    this.setState({ modalIsOpen: true });
  }

  getTask(e){
    this.setState({task : e.target.value});
  }

  getOperation(e){
    this.setState({operation : e.target.value});
  }


  closeModal() {
    this.props.addTask('/api/get_available_service' ,this.state.task, this.state.operation);
    this.props.history.push("/addnew");
    this.setState({ modalIsOpen: false });
  }

  render() {
    const url = ApiConfig.apiBaseUrl + "/api/service_request"   
    const columns = ['jobId', "task", "operation", 'scheduledAt', 'processedAt', 'processStatus', 'retryCount'];
    const options = {
      //  headings: {jobId: '#'},  
      //  sortable: ['jobId', 'scheduledAt'],
      loading: true,
      responseAdapter: function (resp_data) {
        // console.log("data",resp_data)
        return { data: resp_data.serviceRequest, total: resp_data.serviceRequest.length }
      }
    };
    // console.log("aaaaadata", url)
    return(
      <div class="card">
        <div class = "card-header">
          <button type="submit" className="btn btn-primary  float-right" onClick={this.openModal} style={{"marginBottom": "10px" }}>Add New</button>
          </div>
          <div class = "card-body"><ServerTable columns={columns}
            url={url}
            options={options}
            bordered hover
          />
        </div>
        <div>
          <Modal
            isOpen={this.state.modalIsOpen}
            // onRequestClose={this.closeModal}
            ariaHideApp={false}
            style={customStyles}>          
            <h2 >New Task</h2>
            <form>
              <div style={{"marginTop":"20px"}}>
                <label htmlFor="task">Task:</label>
                <div className="col-md-9">
                  <select id="task" name="task" className="form-control" size="1" onChange={this.getTask}>
                    <option value="0">Select your task</option>
                    <option value="ResourceGroup">ResourceGroup</option>
                    <option value="Group">Group</option>
                    <option value="Thing">Thing</option>
                    <option value="ThingType">ThingType</option>
                  </select>
                </div>
              </div>
              <div style={{"marginTop":"20px"}}>
                <label htmlFor="operation">Operation:</label>
                <div className="col-md-9">
                  <select id="Operation" name="operation" className="form-control" size="1" onChange={this.getOperation}>
                    <option value="0">Select your operation</option>
                    <option value="Create">Create</option>
                    <option value="Update">Update</option>
                    <option value="Delete">Delete</option>
                  </select>
                </div>
              </div>
              <div>
                <button className="btn btn-primary float-right" style={{"marginTop":"20px"}} type="button" onClick={() => {this.closeModal()}}>Next>></button>
              </div>
            </form>
          </Modal>
        </div>
      </div>
    );

  }
}
const mapStateToProps = (state)=> {
  return {
    task : state.task,
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addTask,uData
    },
    dispatch
  )
}
// componentWillReceiveProps(){

// }

export default connect(mapStateToProps, mapDispatchToProps)(Task);