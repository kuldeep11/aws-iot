import React, { Component } from 'react';
import { connect } from "react-redux";
import { addTask } from '../../action/task';

class addNew extends Component {
    constructor() {
        super();
        this.state = {
            // loading : true,
            thingName: '',
            thingTypeName: '',
            atts: [{ name: "", age: "" }]
        };
        this.thingName = this.thingName.bind(this);
        this.thingTypeName = this.thingTypeName.bind(this);
        this.attributePayload = this.attributePayload.bind(this);
        this.addAtt = this.addAtt.bind(this);
        this.onCreate = this.onCreate.bind(this);
        // this.addAtt1 = this.addAtt1.bind(this);
    }

    thingName(e) {
        this.setState({ thingName: e.target.value });
    }

    thingTypeName(e) {
        this.setState({ thingTypeName: e.target.value });
    }

    attributePayload(e) {
        if (["name", "age"].includes(e.target.className)) {
            let atts = [...this.state.atts]
            atts[e.target.dataset.id][e.target.className] = e.target.value
            this.setState({ atts }, () => console.log("atts", this.state.atts))
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    }
    addAtt = (e) => {
        // this.setState((prevState) => ({
        //     atts: [...prevState.atts, {name:"", age:""}], }));
    }
    // addAtt1(e){
    //     this.setState({atts : { age: e.target.value }})
    // }
    handleSubmit = (e) => { e.preventDefault() }

    onCreate() {
        console.log("boooooommmmmmm", this.state.thingName, this.state.thingTypeName, this.state.atts)
        // this.props.onCreatetask('/api/service_request');
    }

    // load(){
    //     return new Promise((resolve) => setTimeout(() => resolve(), 2500));
    //   }

    async componentDidMount() {
        await this.props.dispatch(addTask())
        // load().then(() => this.setState({ loading: false }));
    }


    render() {
        let { atts } = this.state
        const { task } = this.props
        const header = task.task && task.task.lastItem
        // console.log("header",header)

        const q = task.task.lastItem && task.task.lastItem.resp && task.task.lastItem.resp.serviceRequest && task.task.lastItem.resp.serviceRequest[0].serviceParameterFormat
        console.log("body", q)

        // var body1 = JSON.parse(q)
        console.log("body1", "body")
        const feildtitle = "Object.keys(q)"
        console.log("booooooooooooooooommm", feildtitle)
        return (
            // <div className="animated fadeIn">
            <div>
                <div class="card">
                    <div class="card-header">
                        <strong>New </strong>
                        <small>Task</small>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="task">Task</label>
                                <input type="text" class="form-control" id="task" value={header && header.data} disabled></input>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="operation">Operation</label>
                                <input type="List" class="form-control" id="operation" value={header && header.oper} disabled></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="thingname">{feildtitle[0]}</label>
                            <input type="text" class="form-control" name="thingName" placeholder="Enter the thing name" onChange={this.thingName.bind(this)} />
                        </div>
                        <div class="form-group">
                            <label for="thingTypeName">{feildtitle[1]}</label>
                            <input type="text" class="form-control" name="thingTypeName" placeholder="Thing Type Name" onChange={this.thingTypeName.bind(this)} />
                        </div>



                        <div class="form-group">
                            <form onSubmit={this.handleSubmit} onChange={this.handleChange} >
                                <label for="attributePayload">{feildtitle[2]}</label>
                                {/* <div class="row">
                                    <div class="form-group col-sm-4">
                                        <label>Key</label>
                                        <input type="text" class="form-control" id="addAtt" name="addAtt" value="addAtt" />
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label>Value</label>
                                        <input type="List" class="form-control" id="addAtt1" name="addAtt1" value="addAtt1" />
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <button onClick={this.addAtt()}>+</button>
                                    </div></div> */}
                                {
                                    atts.map((val, idx) => {
                                        let attId = `att-${idx}`, ageId = `age-${idx}`
                                        return (
                                            <div key={idx}>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        {/* <label htmlFor={attId}>{`att #${idx + 1}`}</label> */}
                                                        <label htmlFor={attId}>Key</label>
                                                        <input type="text" class="form-control" data-id={idx} id={attId} name={attId} value={atts[idx].name} className="name" />
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label htmlFor={ageId}>Value</label>
                                                        <input type="text" class="form-control" data-id={idx} id={ageId} name={ageId} value={atts[idx].age} className="age" />
                                                    </div>
                                                    <div class="form-group col-sm-2">
                                                        <button onClick={this.addAtt()}>+</button>
                                                    </div>
                                                    {/* <label htmlFor={ageId}>Age</label> */}
                                                </div></div>
                                        )
                                    })
                                }</form>

                            {/* <input type="text" class="form-control" id="attributePayload" placeholder="attribute Pay load" /> */}
                        </div>


                        <div class="card-bottom">
                            <button type="submit" class="btn btn-sm btn-primary float-right" onClick={this.onCreate.bind(this)}>Create</button>
                        </div>
                    </div>
                </div></div>
        );
    }
}
const mapStateToProps = state => ({
    task: state.task,
    operation: state.taskList
})
// console.log("taaaaask",task)
// export default addNew;
export default connect(mapStateToProps)(addNew);