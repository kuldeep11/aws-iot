import Constants from '../constants'
const initialState = {
    task: [],
    // taskList: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.ADD_TASK_SUCCESS:
            console.log("success")
            return {
                ...state,
                task: [
                    ...state.task,
                    // ...state.action.payload
                    action.payload,
                    // action.taskName,
                    // action.operation
                ]
            }
        case Constants.ADD_TASK_ERROR:
            console.log("failed",)            
            return {
                ...state
            }
        case Constants.GET_LIST_OF_TASKS_SUCCESS:
            console.log("success")
            return {
                ...state,
                taskList: [
                    ...state.taskList,
                    ...state.action.payload
                ]
            }
        case Constants.GET_LIST_OF_TASKS_ERROR:
            console.log("fail")
            return {
                ...state
            }


        case Constants.POST_ONCREATE_SUCCESS:
            console.log("success")
            return {
                ...state,
                taskList: [
                    ...state.taskList,
                    ...state.action.payload
                ]
            }
        case Constants.POST_ONCREATE_FAIL:
            console.log("fail")
            return {
                ...state
            }

        default:
            return state
    }
    
}