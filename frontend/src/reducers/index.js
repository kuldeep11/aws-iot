import { combineReducers } from 'redux'
import helper from './helper'
import task from './task'
import taskList from './task'

const rootReducer = combineReducers({
    helper, 
    task,
    taskList,
})
 
export default rootReducer