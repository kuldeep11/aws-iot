import Constants from '../constants'
const initialState = {
    loader: false,
    deviceLoader: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.API_REQUEST_START:
            return {
                ...state,
                loader: true
            }
        case Constants.API_REQUEST_END:
            return {
                ...state,
                loader: false
            }        
        default:
            return state
    }
}