const api = {
    post: (url, data) => {
        return axios.post(ApiConfig.apiBaseUrl + url, data)
    },

    get: (url) => {       
        return axios.get(ApiConfig.apiBaseUrl + url)
    },

    put: (url, data) => {
        return axios.put(ApiConfig.apiOosConfigUrl + url, data)
    },

    delete: (url) => {
        return axios.delete(ApiConfig.apiBaseUrl + url)
    }
}

export default api