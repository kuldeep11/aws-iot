from sqlalchemy import create_engine

database = 'iotcli06jan'
mysql_engine = create_engine('mysql+pymysql://{0}:{1}@{2}:{3}'.format('python', 'root', 'db', '3306'))
existing_databases = mysql_engine.execute("SHOW DATABASES;")
existing_databases = [d[0] for d in existing_databases]
if database not in existing_databases:
    mysql_engine.execute("CREATE DATABASE {0}".format(database))
    print("Created database {0}".format(database))
SECRET_KEY = 'p9Bv<3Eid9%$i01'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://python:root@db:3306/{0}'.format(database)