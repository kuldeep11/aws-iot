from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
# from .routes import serviceRequest, serviceAvailable
import json
# local imports
from config import app_config
# db variable initialization
db = SQLAlchemy()
login_manager = LoginManager()


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app.config.from_pyfile('config.py')
    db.init_app(app)
    
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)

    from app import models
    
    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)
    
    from .service import service as service_blueprint
    app.register_blueprint(service_blueprint)
    
    from .aws import aws as aws_blueprint
    app.register_blueprint(aws_blueprint)

    @app.errorhandler(404)
    def not_found(error=None):

        return (json.dumps({'error':True, 'message': 'Not Found: ' + request.url}), 404, {'ContentType':'application/json'})
        
    return app