from flask import flash, request, redirect, render_template, url_for
from flask_login import login_required, login_user, logout_user
import datetime, json
from . import service
from .. import db
from ..models import ServiceRequest, ServiceAvailable

    
@service.route('/api/service_request', methods=['POST', 'GET'])
def RouteServiceRequest():
     ############### POST ##############
    if request.method == 'POST':
        payload = request.get_json()
        try:
            service_request = ServiceRequest(
                containerId = payload.get("containerId"),
                scheduledAt = payload.get("scheduledAt"),
                processStatus = 'pending',
                createdAt = datetime.datetime.now(),
                updatedAt = datetime.datetime.now(),
                task = payload.get("task"),
                operation = payload.get("operation"),
                attrData = json.dumps(payload.get("attrData")),
                retryCount = 0,
                active = 1
            )
            db.session.add(service_request)
            db.session.commit()
            return (json.dumps({'error': False, 'serviceRequest': "Service Request Added"}), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("#error", e)
            return (json.dumps({'error': True, 'message': 'Failed to add Service Request'}), 401, {'ContentType': 'application/json'})

    ############### GET ALL ##############
    elif (request.method == 'GET'):
        try:
            query = ServiceRequest.query.filter_by(active=True).all()
            records = [row.__dict__ for row in query]
            return (json.dumps({'error': False, 'serviceRequest': records}, default=convert_timestamp), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to get Service Request'}), 401, {'ContentType': 'application/json'})


@service.route('/api/service_request/<int:jobId>', methods=['GET', 'PUT', 'DELETE'])
def serviceRequests(jobId):
    ############### GET ##############
    if (request.method == 'GET'):
        try:
            query = ServiceRequest.query.filter_by(jobId=jobId).all()
            records = [row.__dict__ for row in query]
            return (json.dumps({'error': False, 'Service Request': records}, default=convert_timestamp), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to get Service Request'}), 401, {'ContentType': 'application/json'})

    ############### UPDATE ##############
    elif (request.method == 'PUT'):
        payload = request.get_json() if request.get_json() else eval(request.data)
        try:
            get_data = ServiceRequest.query.filter_by(jobId=jobId).first()
            if not get_data:
                return (json.dumps({'error': True, 'message': 'Record does not exist or has been deleted!'}), 404, {'ContentType': 'application/json'})
            query = "UPDATE serviceRequest SET "
            for key, value in payload.items():
                if key in ['jobId', 'containerId', 'scheduledAt', 'attrData', 'createdAt']:
                    return (json.dumps({'error': True, 'message': 'You do not have permission to update' + str(key)}), 200, {'ContentType': 'application/json'})
                else:
                    setattr(get_data, key, value)
            createdAt = get_data.createdAt
            processingTime = (datetime.datetime.now() -
                              createdAt).total_seconds() // 60.0
            get_data.processingTime = processingTime
            get_data.processedAt = datetime.datetime.now()
            get_data.updatedAt = datetime.datetime.now()
            db.session.commit()
            return (json.dumps({'error': False, 'message': 'Updated Successfully'}), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to update Service Request'}), 401, {'ContentType': 'application/json'})

    ############### DELETE ##############
    elif (request.method == 'DELETE'):
        payload = request.get_json()
        try:
            get_data = ServiceRequest.query.filter_by(jobId=jobId).first()
            if not get_data:
                return (json.dumps({'error': True, 'message': 'Record does not exist or already deleted!'}), 404, {'ContentType': 'application/json'})
            get_data.active = False
            db.session.commit()
            return (json.dumps({'error': False, 'message': 'Service Request Deleted Successfully'}), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to delete Service Request'}), 401, {'ContentType': 'application/json'})

@service.route('/api/get_scheduled_task', methods=['GET'])
# @login_required
def getScheduledTask():
    ############### GET ##############
    if (request.method == 'GET'):
        try:
            print ("========", "=================")
            query = ServiceRequest.query.filter(ServiceRequest.active == True, ServiceRequest.processStatus.in_(['pending', 'retry']), ServiceRequest.scheduledAt <= datetime.datetime.now(), ServiceRequest.retryCount <= 5)
            records = [row.__dict__ for row in query]
            return (json.dumps({'error': False, 'serviceRequest': records}, default=convert_timestamp), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to get schedulded task'}))

@service.route('/api/get_available_service', methods=['GET'])
def getAvailableSerice():
    payload = request.args
    if request.method == 'GET':
        try:
            task = payload['task']
            operation = payload['operation']
            serviceName = operation + task
            query = ServiceAvailable.query.filter(ServiceAvailable.serviceName == serviceName, ServiceAvailable.active == True)
            records = [row.__dict__ for row in query]
            return (json.dumps({'error': False, 'serviceRequest': records}, default=convert_timestamp), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to get Available Service'}), 401, {'ContentType': 'application/json'})

def convert_timestamp(item_date_object):
    if isinstance(item_date_object, (datetime.date, datetime.datetime)):
        return item_date_object.isoformat()