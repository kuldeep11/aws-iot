from flask import flash, request, redirect, render_template, url_for
from flask_login import login_required, login_user, logout_user
from . import auth
from .. import db
from ..models import User


# @auth.route('/register', methods=['GET', 'POST'])
# def register():
#     """
#     Handle requests to the /register route
#     """
#     user_payload = request.get_json()
#     print("========", request.get_json(), user_payload.get("password"))
#     # form = RegistrationForm()
#     # if form.validate_on_submit():
#     user = User(
#         email=user_payload.get("email"),
#         username=user_payload.get("username"),
#         first_name=user_payload.get("first_name"),
#         last_name=user_payload.get("last_name"),
#         password_hash=user_payload.get("password")
#     )

#     # add User to the database
#     db.session.add(user)
#     user.password_(user_payload.get("password"))
#     db.session.commit()
#     flash('You have successfully registered! You may now login.')

#     # redirect to the login page
#     # return redirect(url_for('auth.login'))
#     return('You have successfully registered! You may now login.')


# @auth.route('/login', methods=['GET', 'POST'])
# def login():
#     """
#     Handle requests to the /login route
#     Log an User in through the login form
#     """
#     # form = LoginForm()
#     # if form.validate_on_submit():

#     # login_payload = request.get_json()
#     login_payload = request.args
#     # check whether User exists in the database and whether
#     # the password entered matches the password in the database
#     user = User.query.filter_by(email=login_payload.get("email")).first()
#     print ("==Login======", user)
#     if user is not None and user.verify_password(
#             login_payload.get("password")):
#         # log User in
#         login_user(user)

#         # redirect to the dashboard page after login
#         # return redirect(url_for('home.dashboard'))
#         return ("Login Successfully")

#     # when login details are incorrect
#     else:
#         flash('Invalid email or password.')
#         return('Invalid email or password.')

#     # load login template
#     # return render_template('auth/login.html', form=form, title='Login')


# @auth.route('/logout')
# @login_required
# def logout():
#     """
#     Handle requests to the /logout route
#     Log an User out through the logout link
#     """
#     logout_user()
#     flash('You have successfully been logged out.')

#     # redirect to the login page
#     # return redirect(url_for('auth.login'))
#     return('logout')
