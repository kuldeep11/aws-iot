from flask import flash, request
import boto3
import datetime
import json
import logging
import requests
from config import config
import os
import sys
from . import aws
from .. import db
from ..models import Thing, ThingType, User



client = boto3.client('iot', region_name=os.environ['AWS_REGION_NAME'], aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
                      aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'])
# client = boto3.client('iot')
logging.basicConfig(filename='logger.log',
                    format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')

@aws.route('/create_user_thing/<user_id>', methods=['POST', 'GET'])
def create_thing(user_id):
    if request.method == 'POST':
        payload = eval(request.data)
        try:
            thing = Thing(
                user_id = int(user_id),
                thingName = payload.get('thingName'),
                active = 1
            )
            db.session.add(thing)
            db.session.commit()
            return (json.dumps({'error': False, 'message': "Thing Added Successfully"}), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("#error", e)
            return (json.dumps({'error': True, 'message': 'Failed to add Thing'}), 401, {'ContentType': 'application/json'})
        
@aws.route('/delete_user_thing/<user_id>', methods=['POST', 'GET'])
def delete_thing(user_id):
    if request.method == 'POST':
        payload = eval(request.data)
        try:
            get_data = Thing.query.filter_by(thingName=payload.get('thingName')).first()
            if not get_data:
                return (json.dumps({'error': True, 'message': 'Record does not exist or already deleted!'}), 404, {'ContentType': 'application/json'})
            get_data.active = False
            db.session.commit()
            return (json.dumps({'error': False, 'message': 'Thing Deleted Successfully'}), 200, {'ContentType': 'application/json'})
        except Exception as e:
            print("=>", e)
            return (json.dumps({'error': True, 'message': 'Failed to delete Thing'}), 401, {'ContentType': 'application/json'})


@aws.route('/list_user_things/<int:user_id>', methods=['POST', 'GET'])
def list_things(user_id):
    query = Thing.query.filter_by(user_id=user_id, active=True).all()
    records = [row.__dict__ for row in query]
    # del records['_sa_instance_state']
    for thing in records:
        response = client.list_things(
            maxResults=123,
            # attributeName='thingName',
            # attributeValue='Demo',
            thingTypeName=thing['thingName'],
        )
        print (records)
        return response

