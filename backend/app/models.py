from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import uuid
from app import db, login_manager


class User(UserMixin, db.Model):
    """
    Create an User table
    """

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    password_hash = db.Column(db.String(128))
    plan_id = db.Column(db.Integer)
    plan_valid_till = db.Column(db.DateTime)
    plan_hits_remaining = db.Column(db.Integer)
    last_login = db.Column(db.DateTime)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    things = db.relationship('Thing', backref='thing',
                                lazy='dynamic')
    thing_types = db.relationship('ThingType', backref='thingtype',
                                lazy='dynamic')
    serviceRequest = db.relationship('ServiceRequest', backref='serviceRequest', lazy='dynamic')
    active = db.Column(db.Boolean, default=True)

    # @property
    # def password(self):
    #     """
    #     Prevent pasword from being accessed
    #     """
    #     print("=============-----")
    #     raise AttributeError('password is not a readable attribute.')

    # @password.setter
    def password_(self, password):
        """
        Set password to a hashed password
        """
        print ("=========************ Pass", generate_password_hash(password))
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<Employee: {}>'.format(self.username)


# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(id))

class ServiceRequest(UserMixin, db.Model):
    """
    Create a ServiceRequest table
    """
    
    __tablename__ = 'serviceRequest'
    def generate_uuid(self):
        return str(uuid.uuid4())

    jobId = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(150), default=generate_uuid)
    task = db.Column(db.String(60))
    operation = db.Column(db.String(60))
    userId = db.Column(db.Integer, db.ForeignKey('users.id'))
    containerId = db.Column(db.String(60))
    scheduledAt = db.Column(db.DateTime)
    processedAt = db.Column(db.DateTime)
    processStatus = db.Column(db.String(60))
    retryCount = db.Column(db.Integer, default=0)
    attrData = db.Column(db.String(350))
    processingTime = db.Column(db.Integer)
    response = db.Column(db.String(60))
    createdAt = db.Column(db.DateTime)
    updatedAt = db.Column(db.DateTime)
    active = db.Column(db.Boolean, default=True)


    def __repr__(self):
        return '<Service Request: {}>'.format(self.jobId)



class ServiceAvailable(UserMixin, db.Model):
    """
    Create a ServiceAvailable table
    """
    __tablename__ = 'serviceAvailable'

    id = db.Column(db.Integer, primary_key=True)
    serviceName = db.Column(db.String(60), index=True)
    serviceCommand = db.Column(db.String(60), index=True)
    serviceParameterFormat = db.Column(db.String(60), index=True)
    serviceParameterRequired = db.Column(db.String(60), index=True)
    serviceParameterOptional = db.Column(db.String(60), index=True)
    active = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __repr__(self):
        return '<Service Available: {}>'.format(self.serviceCommand)

    
class SubscripionPlans(UserMixin, db.Model):
    """
    Create a ServiceRequest table
    """

    
    __tablename__ = 'subscripionPlans'

    planId = db.Column(db.Integer, primary_key=True)
    planName = db.Column(db.String(60))
    planValidityDays = db.Column(db.Integer)
    planValidityHits = db.Column(db.Integer)
    dateAdded = db.Column(db.DateTime)
    active = db.Column(db.Boolean, default= True)


    def __repr__(self):
        return '<Service Request: {}>'.format(self.planName)
    
class Thing(UserMixin, db.Model):
    """
    Create a Thing table
    """
    __tablename__ = 'thing'

    thingName = db.Column(db.String(60), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    version = db.Column(db.Integer, default= True)
    active = db.Column(db.Boolean, default= True)

    def __repr__(self):
        return '<Thing: {}>'.format(self.thingName)

class ThingType(UserMixin, db.Model):
    """
    Create an Thing Type table
    """

    __tablename__ = 'thingtype'

    thingTypeName = db.Column(db.String(60), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    active = db.Column(db.Boolean, default= True)

    def __repr__(self):
        return '<Thing Type: {}>'.format(self.thingTypeName)